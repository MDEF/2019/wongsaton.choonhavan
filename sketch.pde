float y = 100;
 
// The statements in the setup() function 
// run once when the program begins
void setup() {
  size(360, 360);  // Size should be the first statement
  stroke(255);     // Set stroke color to white
  noLoop();
  
  y = height * 0.9;
}

// The statements in draw() are run until the 
// program is stopped. Each statement is run in 
// sequence and after the last line is read, the first 
// line is run again.
void draw() { 
  background(0);   // Set the background to black
  
    fill(180,y,200);
  rect(0, y, width, y+100);  
  
      fill(200,y,y);
  ellipse(180, y, width, y-200);
  
  y = y - 1; 
  if (y < -200) { 
    y = height+200; 
  } 
} 

void mousePressed() {
  loop();
}